## 現場から学ぶ
## Django開発のノウハウ

2018/08/04

keinuma

---?include=common/who_are_you.md

---

### 今日話すこと

- 独学開発ってムズカシイ
- 転職後のDjango開発の変化

Note:
- 直近半年程度独学でWebアプリを開発
- 実際に独学で開発した経験を元に発表

---

## 独学開発ってムズカシイ

Note:
- 開発からデプロイまでの範囲

+++

### 学ぶ範囲が広い
- Python
- Webのきほん
- Django
- データベース
- etc...

Note:
- とにかくやることが多い
- つまづいた時にWebとフレームワークどちらのの知識が足りてないのか
- 今ならFirebaseなどあるが、バックエンドを勉強したい

+++

### インフラ構築がわからない
- クラウドはAWS？GCP？
- Docker？VirtualBox？


Note:
- 色々な人からどちらが良いかきく
- 直近で両方経験したが、個人プロジェクトの規模ならGCPか

+++

### ベストプラクティスがわからない
- ディレクトリ構成って何が正しいの？
- テストはどう書くの？

Note:
- 今日の本題
- 細かいテクニックや範囲外の話　　

+++


一人では守備範囲に限界がある  
→エンジニアに転職してみた

Note:
- 転職すると、実際のプロダクトを見ることができる
- コードレビューを受けることができる（厳しいほど嬉しい）

---

## 転職後のDjango開発の変化

Note:
- 個人プロジェクトでのDjangoの書き方の変化

---

## settings.py

Note:
- アプリケーションの設定ファイル
- ロギングや認証方法など

+++

### 転職前
- 環境ごとに用意
- dev.pyやproduction.py

Note:
- 具体的なイメージはこちらになる

+++
    
![Logo](assets/images/20180803_before_settings.png)

Note:
- 修正が入ると辛い
- どの環境をいじらないといけないのか考慮

+++

### 転職後
- settings.pyは一つ
- 環境の差異は環境変数で吸収
- `django-environ`の活用

Note:
- 環境変数で変化を吸収する
- AWSのSecretsManagerやGKEのSecretsに格納する

+++

![Logo](assets/images/20180803_after_settings.png)

Note:
- ファイルがすっきりして、心が穏やか
- コードの修正は安心してできる

+++

### 例
```python
import os
import environ

# プロジェクトのルートディレクトリ
BASE_DIR = str(environ.Path(__file__) - 3)
env = environ.Env()
READ_ENV_FILE = env.bool('DJANGO_READ_ENV_FILE', default=False)

if READ_ENV_FILE:
    # 開発環境の場合はファイルから読み込む
    env_file = os.path.join(BASE_DIR, 'develop.env')
    env.read_env(env_file)

ALLOWED_HOSTS = env('ALLOWED_HOSTS', default=[])
```

@[9-12]
@[14]

Note:
- environはdjango-environ
- READ_ENV_FILEはローカルの時にファイルから読み込む
- 本番環境では環境変数から読み込む

---

## Unit Test

Note:
- 個人開発では書く気が起こらないもののひとつ
- TDDまではいかないが、書いとくといつか助かる

+++

### 転職前
- `django.test.TestCase`を使用
- 事前のデータ準備はテスト内に記述

Note:
- テストのためのデータをどう準備するか

+++

ありがちなAPIのテスト

```python
from django.test import TestCase
from .models import MyModel


class APITestCase(TestCase):
    def test_view(self):
        data = {'name': 'test', 'number': 1}
        self.client.post('/api/model/', data=data)
        response = self.client.get('/api/model/')
        assert response.json() == self.data

```

@[8]

Note:
- class単位でまとめるので一つのファイルが大きくなりがち
- GETするAPIを叩いて値が入っているかのテスト
- データを準備するためにAPIを叩いているアンチパターン

+++

### 転職後
- `pytest` + `pytest_django`
- factory_boy を活用

Note:
- pytestは便利
- pytestの本が出るらしいので楽しみ

+++

### factory_boy
- Djangoのモデルを用意してくれる
- SQLAlchemyなども対応

```python
import factory


class MyModelFactory(factory.django.DjangoModelFactory):
    name = 'Name'
    number = 12

    class Meta:
        model = "myapp.MyModel"

```

Note:
- factory_boyはデータを事前に準備できるもの
- デフォルト値を定義できる、かつモデル作成じに代入することもできる

+++

```python
import pytest
from .my_factory import MyModelFactory

    
@pytest.mark.django_db
@pytest.mark.parametrize('data', (
    {'name': 'test', 'number': 1},
    {'name': 'test2', 'number': 2},
))
def test_view(self, data):
    MyModelFactory(**data)
    response = self.client.get('/api/model/')
    assert response.json() == self.data

```


@[5]
@[6-9]
@[11]

Note:
- 
- pytestはデコレータで使用
- django_dbでテスト用のモデルを作成可能
- parametrizeで一つのテストケースに複数のinputを試せる
- ファクトリーを呼び出すことで、モデルに保存できる

---

### まとめ

- Twelve Factor appに従う
    - https://12factor.net/ja/
- 迷ったら現場で使ってる人に聞く


Note:
- 今までの手法はTwelve Factor appに則っている
- クラウドアプリケーション開発するときのルール
- どんな技術でも現場でつかった人に聞くと確実

---

### ちなみに
- 転職にはforkwellを利用させていただきました
- https://forkwell.com/

Note:
- エージェント、ジョブス、ポートフォリオ全て利用しました
- 大変お世話になりました
- その節はありがとうございました

---

ご静聴ありがとうございました。
