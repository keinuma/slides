## Pythonもくもく自習室

2018/09/01

沼田(keinuma)


---?include=common/who_are_you.md

---

## 宣言したこと

- Django + Vue のアプリケーション作り

---

## 作っているもの
- 自分のブログアプリケーション
    - アウトプット不足
    - インフラの良い練習台

---

## 今日やったこと
- バックエンド、フロントエンドのCI/CD設定
- vue-cliのバージョンアップ

---

## CI/CDまわり
- Host: GitLab
- CI: GitLab-CI
- 環境構築: Docker

---

## バックエンド
- Flake8 -> pytest -> GCRにpush
- GCPへの認証方法に迷った
- GCRとかくとGitLabかGCPかわからない

---

## フロントエンド(Vue)
- Jest -> Build -> GCSにコピー
- npmが重い…
- vue-cliの3系は神

---

## vue-cli ver3
- process.envのサポート
- プロジェクト管理UIの追加
- buildやrunをvue-cli-serviceで管理

---

## 今日の所感
- GitLabで一貫してCIできるの良い
- vue-cliが便利
- ランチでクラウドの話聞けてよかった

---

ご静聴ありがとうございました。
