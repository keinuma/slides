## v-modelのお話

2018/08/25 「人がいないと勉強できない」

沼田(keinuma)


---?include=common/who_are_you.md

---

### v-model使ってますか？

---

### 属性ひとつで

```html
<input v-model="message">
```

---

### いい感じにやってくれる

1. データを参照して、HTML内にバインディング
2. inputイベントからデータを更新

---

### つまり

---

- `v-bind`でデータバインド
- `v-on`でデータの更新

```html
<input
    v-bind:value="message"
    v-on:input="message = $event.target.value">

```

---

### よくわからないこと
- 変数の型をどうみているのか

---

### 複数セレクトボックス
```html
    <label>
        <select v-model="val" multiple>
            <option value="a">A</option>
            <option value="b">B</option>
            <option value="c">C</option>
        </select>
    </label>
    <p>val: {{ val }}</p>
```
---

`val` の初期型 -> Array

---

#### 選択
- A, B, C

<br/>

#### 結果
- `val = ["a", "b", "c"]`

---

`val` の初期型 -> String

---

#### 選択
- A, B, C

<br/>

#### 結果
- `val = true`

---

### デモ

---

### Vueの中身を調べる

---

#### v-modelディレクティブが呼ばれているとこ

---

タグをみて処理を変えている
```javascript
  if (el.component) {
    genComponentModel(el, value, modifiers);
    return false
  } else if (tag === 'select') {
    genSelect(el, value, modifiers);
  } else if (tag === 'input' && type === 'checkbox') {
    genCheckboxModel(el, value, modifiers);
  }
```

---

CheckBoxだったら

---

valueの型がArrayか判断 -> Array以外は受け付けない?
```javascript
    if(Array.isArray($$a)){ +
      var $$v= + (number ? _n( + valueBinding + ) : valueBinding) + , +
          $$i=_i($$a,$$v); +
      if($$el.checked){$$i<0&&( + (genAssignmentCode(value, $$a.concat([$$v]))) + )} +
      else{$$i>-1&&( + (genAssignmentCode(value, $$a.slice(0,$$i).concat($$a.slice($$i+1)))) + )} +
    }else{ + (genAssignmentCode(value, $$c)) + },
```

---

SelectBoxだったら

---

multipleが付いているかで、処理を判断
```javascript
  var assignment = '$event.target.multiple ? $$selectedVal : $$selectedVal[0]';
```

---

objectの場合

---

'`object.key`'を文字列で受け取り、`.`の有無で判断
```javascript
  if (val.indexOf('[') < 0 || val.lastIndexOf(']') < len - 1) {
    index$1 = val.lastIndexOf('.');
    if (index$1 > -1) {
      return {
        exp: val.slice(0, index$1),
        key: '"' + val.slice(index$1 + 1) + '"'
      }
    }
}
``` 

---

### まとめ
- v-modelは全ての型を受け付けるわけではない
- タグによってパース方法が異なる
- Vue -> Javascriptのパース処理は難しい…

---

ご静聴ありがとうございました。