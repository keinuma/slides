# スライド資料置き場

## 構成
- GitPitchで作成
- 発表ごとにディレクトリを作成

## 過去発表

- [基礎から学ぶVue.js Chapter1](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180716_hitogainaito)
- [GsPython LT大会](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180804_GsPython)
- [shinjukumokumoku](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180818_shinjukumokumoku)
- [基礎から学ぶVue.js Chapter3](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180825_hitogainaito)
- [Pythonもくもく自習室](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180901_rettypy)
- [もぐもぐDjango](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180908_mogumogu_django)
- [Pythonもくもく自習室](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20180922_rettypy)
- [基礎から学ぶVue.js Chapter5](https://gitpitch.com/keinuma/slides?grs=gitlab&p=20181006_hitogainaito)
