## Shinjukuもくもく

2018/08/18

keinuma

---?include=common/who_are_you.md

---

### 今日宣言したこと

- Vue + Djangoのアプリ開発
- VueのLT資料準備

Note:
LT資料は全然できていない

---

## 作ってるもの

- 自分のブログアプリケーション
    - アウトプット不足
    - インフラの良い練習台

Note:
- 開発からデプロイまでの範囲
- DjangoとVuejsかつTypescript

---

## 作業範囲
- ブログのUI作り(Typescriptを使用)
    - APIとの連携
    - UIライブラリ使って整える
- APIの修正

---

### Typescriptが慣れない

---

普通のVuejs

```javascript
export default {
    name: 'HeaderPartsWord',
    props: {
        name: String,
        link: String
    }
}
```

---

Vuejs + Typescript

```typescript
import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class HeaderPartsWord extends Vue {
    @Prop(String) public name!: string;
    @Prop(String) public link!: string;
}
```

---

- デコレータが必要
- ラストカンマが強制（デフォルトLinter）
- 初期化しない場合は変数の後ろに `!` が必要

---

## UIライブラリの選定

---

- [Vuetify](https://vuetifyjs.com/ja/getting-started/quick-start): マテリアルデザイン
- [ElementUI](http://element.eleme.io/#/en-US): 人気らしい
- [Bulma](https://bulma.io/): カスタマイズ性が良い
- [Buefy](https://buefy.github.io/#/): Vue向けBulma

---

SelectBoxで比較してみる

---

Vuetify
```html
<v-select
    :items="items"
    label="Standard">
</v-select>
```

---

Bulma
```html
<div class="select">
  <select>
    <option>Select dropdown</option>
    <option>With options</option>
  </select>
</div>
```

---

- Vuefyは専用のタグを使用
- Bulmaはclassに値を入れる

→ Bulmaの方が良さそう

---

今日はここまで