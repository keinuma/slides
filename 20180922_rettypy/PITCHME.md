## Pythonもくもく自習室

2018/09/22

沼田(keinuma)


---?include=common/who_are_you.md

---

## 宣言したこと

- PyCon JP 2018レポート
- 逃したトークのyoutubeを眺める

---

## 参加レポート

---

- 自作ブログアプリ
- https://blog.keinuma.com/

---

バグってます

---

### 改善点
- ブラウザバックすると戻らない
- Netlifyでvue-routerのHistoryモードを設定できない
- リンクのプレビューを表示したい
- 画像挿入機能がない

---

先が長い...

---

## 今日見たトーク

---

### Pythonでざっくり学ぶUnixプロセス
- プロセス周りをPython使って解説
- わかりやすい

---

### Python2 から Python3 への移植: MonotaRO での取り組み
- お昼時に話題に上がったので拝聴
- 同一リポジトリでの移行に納得

---

ご静聴ありがとうございました。
