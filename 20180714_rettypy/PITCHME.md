## Pythonもくもく自習室

2018/07/14

沼田(keinuma)


---?include=common/who_are_you.md

---

## やったこと

---

### `Djangoの管理者画面のカスタマイズ`

- 自分のモデルデータに外部APIから取得したデータをマージして出力したい
- 一般に公開するのではなく、Admin内で管理したい


---

### `自作アプリをpipenvで管理する`
- requirementsの分離が辛くなってきた
- pip freezeは使いたいパッケージなのか依存パッケージなのかわからなくなってきた


---

### pipenvでつまづいたこと
- プロジェクト内に仮想環境が構築されない
    - 環境変数に `PIPENV_VENV_IN_PROJECT=true` を追加
- Dockerを使うときに仮想環境を作らない
    - `pipenv install --system`

---
### pipenv入れてよかったこと
- 開発環境と本番環境のパッケージをフラグで切り分けれる
- .envを読み込んでくれる
- 依存関係の確認が楽
    - `pipenv graph`
    - `pipenv graph reverse`
