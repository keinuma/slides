## ElementUIを使ってみた

2018/07/16 「人がいないと勉強できない」

沼田(keinuma)


---?include=common/who_are_you.md

---

### ElementUIてなに？？

---

### 「A Desktop UI Library」
- http://element.eleme.io/#/en-US

- 「基礎から学ぶVue.js」 Chapter1: 33P
- Vue.jsのコンポーネントライブラリ
- 画像のアップローダーやスライダーなど多機能UIが揃っている


---

![Logo](assets/images/thinking-face_1f914.png)

---

### 開発中の書籍管理アプリに導入してみる

---

### 書籍検索ページ

![Logo](assets/images/20180716_all_vue.png)

---

### 1. フォーム

![Logo](assets/images/20180716_search_form.png)

```html
<el-form :inline="true" :model="searchForm" label-width="120px">
  <el-form-item label="キーワード">
    <el-input placeholder="Vue.js" v-model="searchForm.keyword"></el-input>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="getBooks" icon="el-icon-search">検索</el-button>
  </el-form-item>
</el-form>
```

@[1]
@[2]
@[3]
@[5-7]

---

### 2. テーブル

![Logo](assets/images/20180716_search_table.png)

```html
    <el-table
      :data="response"
      :default-sort="{prop: 'releaseDate', order:'descending'}">
      <el-table-column
        prop="title"
        label="タイトル"
        sortable
        width="300" >
      </el-table-column>
    </el-table>
```

@[1-3]
@[4-9]

---

### 3. ページネーション

![Logo](assets/images/20180716_search_pager.png)

```html
    <el-pagination
      background
      v-show="response.length > 0"
      @current-change="changePage"
      :page-count="maxPage"
      :current-page="page">
    </el-pagination>
```

@[1-6]
@[2]
@[3]
@[4]
@[5]
@[6]

---

### CSS苦手マンにはありがたい

---

### 他のUIライブラリ

- [Vuetify](https://vuetifyjs.com/ja/)
- [BULMA](https://bulma.io/)

---

### ご静聴ありがとうございました