## Atomic Designを試す

<div class="author">
沼田(keinuma)
</div>
<div class="date">
2018/10/06  
【Chapter5】超超初心者ワイワイVue.js勉強会
</div>


---?include=common/who_are_you.md

---

### アジェンダ
- Atomic Designの概要
- VueとAtomic Design
- 実践した感想

---

### Atomic Designとは

> コンポーネント単位で設計するUIデザイン  
> パーツの最小単位で分離  
> 再利用可能にする

---

![Logo](assets/images/atomic-design-process.png)

---

### もっと詳しく
- Atoms: 分割できない最小単位
- Molecules: Atomsの組み合わせ
- Organism: Atoms, Moleculesの組み合わせ
- Template: ワイヤー
- Pages: ページ

---

### なぜAtomic Designを使うか
- 大規模開発におけるコードの分離
- 一つの指針を立てて、チームの認識を合わせる

---

### VueとAtomic Design
- pagesはルーティングに対応
- CSSは`scoped css`を活用
- データのやり取りはpropsイベントとVuexを使い分ける

---

### 良かったところ
- コンポーネントを作りやすい
- デザインの変更がしやすい
- 状態を追加しやすい

Note:
依存関係が明確でコンポーネントを作り込みやすい

---

### 難しいところ
- ファイル数多い問題
- HTML・CSSのコーディング手法が変わる
- 用語が浸透されていない
- MoleculesとOrganismの区別がつかない
- どこまで厳密にやるか

---?image=assets/images/vue-book.jpg&size=30% 60%

輪読会の共催者を募集しています

---

### ご静聴ありがとうございました。