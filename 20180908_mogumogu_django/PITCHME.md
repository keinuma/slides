## もぐもぐDjango

2018/09/08

沼田(keinuma)


---?include=common/who_are_you.md

---

## 宣言したこと

- 自作アプリをKubernetesにデプロイ

---

## 作っているもの
- 自分のブログアプリケーション
    - アウトプット不足
    - インフラの良い練習台

---

## 今日やったこと
- KubernetesのYaml書くマン
- CloudSQLの設定
- CloudStorageの設定

---

## Kubernetesの設定
- 環境変数のセッティング(ConfigMap, Secrets)
- Deploymentの作成 <- ハマった
- Servicesの作成

---

## Deploymentの作成
- 名前空間の定義が多すぎて混乱
- metadata?label?annotation?

- CloudSQLとの接続がめんどくさい


---

![Logo](assets/images/20180908_god_tweet.png)

---

@size[3.5em](🙇‍)

---

無事デプロイ完了

---

## CloudSQLの設定
- cloud_sql_proxyでつなぐ
- Imageを落とさないといけない

---

```yaml
        - name: cloudsql-proxy
          image: b.gcr.io/cloudsql-docker/gce-proxy:1.05
          command: ["/cloud_sql_proxy", "--dir=/cloudsql",
                    "-instances=<project-id>=tcp:5432",
                    "-credential_file=/secrets/cloudsql/credentials.json"]
          volumeMounts:
            - mountPath: /secrets/cloudsql/
              name: cloudsql-oauth-credentials
              readOnly: true
            - name: ssl-certs
              mountPath: /etc/ssl/certs
            - name: cloudsql
              mountPath: /cloudsql
```

---

もっと楽にしたい…

---

## カイゼンしたいこと
- CloudSQLとの接続を簡易化
- Secretsの管理方法の確立

---


ご静聴ありがとうございました。